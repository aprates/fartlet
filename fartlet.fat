#!/usr/bin/env fry

# FatScript port of FIGlet/TOIlet (https://fatscript.org)

# FARTlet project <https://gitlab.com/aprates/fartlet>
# Original author: Antonio Prates
# License: MIT

_       <- fat.type.List
_       <- fat.type.Text
_       <- fat.type.Number
file    <- fat.file
system  <- fat.system
console <- fat.console
curses  <- fat.curses
math    <- fat.math

# Configuration
~ fontsPathBase = 'fonts/'  # path where the fonts are stored

# Method to print FARTlet cli options
showHelp = (): Void -> {
  console.log('usage: ./fartlet.fat <font> words.. (print words with font)')
  console.log('or...: ./fartlet.fat all words.. (print words with all fonts)')
}

# Ordered valid/printable ascii characters [32-127]
asciiChars =
  ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
  '[\\]^_`abcdefghijklmnopqrstuvwxyz\{|}~'

# Glyph stores character representation as multi-line fragments
Glyph = List/Text

# Method to read a flf file and extract font glyphs and meta
# implicit arg: fontsPathBase (from global scope)
loadFont = (fontFile: Text): Scope -> {
  rawFontData = file.read(fontFile)

  rawFontData == Void ? {
    console.log('** FARTlet cannot find the requested font: {fontFile}', 3)
    showHelp()
    system.exit(exitFailure)
  }

  lines = rawFontData.split('\n')
  params = lines(0).split(' ')
  hardblank = params(0)(-1)
  height = Number(params(1))
  headerSize = Number(params(5))

  characters: Scope/Glyph = {}
  ~ index = 1 + headerSize
  ~ widths = []

  # Map valid/printable ascii codes [32-127] to font glyphs
  32..127 @ asciiCode -> {
    representation: Glyph = (lines(index..index + (height - 1))) @ frag -> {
      frag - '@'  # remove end-of-line markers
    }

    widths += [ representation(0).size ]  # store width of each glyph
    characters.[asciiCode] = representation
    index += height
  }

  validContent = lines(1 + headerSize..-2)

  # Print font info when listing
  slug == 'all' ? {
    name = fontFile(fontsPathBase.size..fontFile.size - 5)
    width = math.mean(widths).format('%.0f')
    meta = headerSize > 2 ? lines(2) : '-'
    console.log(
      '\nfont: {name} | average glyph size: {width}x{height} | meta: {meta}'
    )
  }

  # Warn about ignored glyphs
  validContent.size > 95 * height ?
    console.log('** FARTlet will ignore special chars for: {fontFile}', 3)

  { characters, hardblank, height }
}

# Method that retrieves the ASCII code for a given character
getAsciiCode = (char: Text): Number -> {
  index = asciiChars.indexOf(char)

  index >= 0 => index + 32
  _          => 0
}

# Method that renders representation of the given text using a flf font
renderText = (fontFile: Text, text: Text, maxWidth: Number) -> {
  { characters, hardblank, height } = loadFont(fontFile)

  lines = 1..height @ -> { { ~ output = '' } }

  text.split('') @ char -> {
    asciiCode = getAsciiCode(char)
    asciiCode ? {
      ..<height @ i -> {
        lines(i).output += characters(asciiCode)(i)
      }
    }
  }

  (lines @ -> _.output.replace(hardblank, ' ')(..maxWidth)).join('\n')
}

# Method to check if dealing with a font file
isFlfFile = (filename: Text): Boolean -> {
  filename(-4..) == '.flf'
}

# Main program
main = -> {
  args = system.args

  args.size < 2 ? {
    showHelp
    system.exit(exitFailure)
  }

  slug = args(0)
  text = args(1..).join(' ')

  maxX = curses.getMax.x - 1
  curses.endCurses

  slug == 'all' ? {
    all = file.lsDir(fontsPathBase).sort
    all @ filename -> {
      isFlfFile(filename) ?
        console.log(renderText(fontsPathBase + filename, text, maxX))
    }
  } : console.print(renderText(fontsPathBase + slug + '.flf', text, maxX) + '\n')
  exitSuccess
}

$isMain ? main()
