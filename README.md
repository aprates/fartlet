# FARTlet ‍💨

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

`FARTlet` is a FatScript port of FIGlet/TOIlet. It enables users to generate large letters from ordinary ASCII text, offering multiple font support.

## Installation

To use `FARTlet`, ensure you have `fry` (>2.2.0), the [FatScript](https://fatscript.org) interpreter installed on your system.

**Clone this repository:**

```bash
git clone https://gitlab.com/aprates/fartlet.git
cd fartlet
```

OR

**Use the [chef](https://gitlab.com/fatscript/chef) package manager**:

```bash
chef menuadd fatscript.org
chef include fartlet
chef restock
```

> when using `FARTlet` as a library on another project

After completing the above steps, you can use `FARTlet` to generate text in large letters!

## How to Use

Once cloned, invoke `FARTlet` from the project root as shown below:

```bash
chmod +x *.fat
./test.fat
# OR
./fartlet.fat [font] [text]
```

## Font File Explanation

The initial line of `.flf` files comprises:

- `flf2a` - A "magic number" for identifying file format.
- `$` - A "hardblank", which is visualized as a blank space but isn't subject to smushing.
- `n` - The character's height.
- `n` - Character height, without descenders (`FARTlet` overlooks this).
- `n` - The longest possible line length (not considered by `FARTlet`).
- `n` - The font's default smushmode (again, ignored by `FARTlet`).
- `n` - Count of comment lines.

## Future plans

I'am not planning any updates... That said, contributions are welcome! The current implementation has a few limitations, being the most relevant not having smushmodes support. Yet, here are some cool ideas for any further development on this project, for anyone wanting to give it a try:

- Add support for smushmodes
- Add multiline support
- Add font coloring features

## Contributing

If you have suggestions for new features or improvements, please open an issue on the [FARTlet GitLab](https://gitlab.com/aprates/fartlet/issues).

### Donations

If you find FARTlet useful and would like to support its development, you can [Buy me a coffee](https://www.buymeacoffee.com/aprates).

## Licensing

- The project's software/code holds an [MIT License](LICENSE) © 2023-2024 Antonio Prates.
- Fonts sourced from the FIGlet website are under the [FIGlet License](fonts/FIGLET_LICENSE).
- "Future", "Pagga", and "Smblock" fonts from TOIlet have the [WTFPL License](fonts/TOILET_LICENSE).
